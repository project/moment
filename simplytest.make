api = 2
core = 8.x

libraries[moment][download][type] = "get"
libraries[moment][download][url] = "https://github.com/moment/moment/archive/2.12.0.tar.gz"
